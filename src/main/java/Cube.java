public class Cube implements Figure3d {
    private double side;

    public Cube(double oneSide){
        side = oneSide;
    }

    public double getArea() {
        return 6 * Math.pow(side, 2);
    }

    public double getVolume() {
        return Math.pow(side, 3);
    }

    public double getPerimeter() {
        return 12 * side;
    }
}
