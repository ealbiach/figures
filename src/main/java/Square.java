public class Square implements Figure2d{
    private double side;

    public Square(double oneSide){
        side = oneSide;
    }

    public double getArea(){
        return side * side;
    }

    public double getPerimeter(){
        return 4 * side;
    }

}
